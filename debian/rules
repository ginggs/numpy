#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

# The following tests are problematic and skipped:
# - Meson's python module always returns the default interpreter, which makes
#   Cython related tests fail on non-default python versions
# - numpy.distutils is still shipped because of Python 3.11, which makes the
#   test_all_modules_are_expected test fail with Python 3.12+ (where it is
#   supposed to be dropped).
# - Some f2py tests have architecture-specific failures. Further investigation
#   is needed.
# - There seems to be an issue with floating point exceptions on armhf/armel
# - At least one mips64el buildd has issues with NaN values
SKIP_TESTS = \
	cython \
	test_all_modules_are_expected
ifeq ($(filter amd64 arm64 ppc64el,$(DEB_HOST_ARCH)),)
SKIP_TESTS += f2py
endif
ifneq ($(filter riscv64 armhf armel,$(DEB_HOST_ARCH)),)
SKIP_TESTS += FPClass test_float
endif
ifneq ($(filter armhf armel,$(DEB_HOST_ARCH)),)
SKIP_TESTS += error exception warning fpe overflow \
	SpecialFloats \
	test_divide_err \
	test_empty \
	test_errobj \
	test_features \
	test_invalid \
	test_log2_special \
	test_loss_of_precision \
	test_nonarray_assignment \
	test_special_values \
	test_where \
	test_zero_power_nonzero
endif
ifneq ($(filter mips64el,$(DEB_HOST_ARCH)),)
SKIP_TESTS += error Special test_reduce Nan
endif
ifneq ($(filter ppc64 s390x,$(DEB_HOST_ARCH)),)
SKIP_TESTS += test_impossible_feature_enable
endif
ifneq ($(filter s390x,$(DEB_HOST_ARCH)),)
SKIP_TESTS += test_linear_interpolation_formula_symmetric
endif

export PYBUILD_TEST_CUSTOM = 1
export PYBUILD_TEST_ARGS = {interpreter} {dir}/runtests.py --no-build --verbose --pythonpath {build_dir} \
	-- $(if $(SKIP_TESTS),-k 'not ($(call concat_with,$(space)or$(space),$(SKIP_TESTS)))')
export PYBUILD_BUILD_ARGS = --config-setting build-dir={dir}/.mesonpy

ifneq ($(DEB_HOST_ARCH),$(DEB_BUILD_ARCH))
export PYBUILD_BEFORE_BUILD = meson env2mfile --debarch $(DEB_HOST_ARCH) --cross -o {dir}/debian/meson-cross.txt \
	&& python3 {dir}/debian/helpers/add_longdouble_format_property.py {dir}/debian/meson-cross.txt $(DEB_HOST_ARCH)
export PYBUILD_AFTER_BUILD = rm {dir}/debian/meson-cross.txt
PYBUILD_BUILD_ARGS += --config-setting setup-args=--cross-file={dir}/debian/meson-cross.txt
endif

# Helper variables
PY3VERS = $(shell py3versions -vr)
STAGING_DIR = $(CURDIR)/debian/python3-numpy
space = $(eval) $(eval)
concat_with = $(subst $(space),$1,$2)

%:
	dh $@ --buildsystem=pybuild

execute_after_dh_python3:
	# tweak the entry_points console_scripts list to include all supported versions
	if [ -f $(STAGING_DIR)/usr/lib/python3/dist-packages/numpy-*.dist-info/entry_points.txt ] ; then \
		ENTRYPOINT=$(shell ls $(STAGING_DIR)/usr/lib/python3/dist-packages/numpy-*.dist-info/entry_points.txt) ; \
		python3 debian/helpers/edit_console_scripts.py $$ENTRYPOINT "f2py3" "numpy.f2py.f2py2e:main" ; \
		for v in $(PY3VERS); do \
			python3 debian/helpers/edit_console_scripts.py $$ENTRYPOINT "f2py$$v" "numpy.f2py.f2py2e:main" ; \
		done \
	fi
	rm -rf $(STAGING_DIR)/usr/lib/python3.*

execute_before_dh_install:
	# add versioned f2pyX.Y scripts
	cp -a $(STAGING_DIR)/usr/bin/f2py $(STAGING_DIR)/usr/bin/f2py3
	# Create wrappers to run python$$i without dh-python generating a
	# hard dependency on all of those interpreters (#945824)
	set -e; for v in $(PY3VERS); do \
		sed -e "s,@VERSION@,$$v,g" debian/f2py.in > $(STAGING_DIR)/usr/bin/f2py$$v; \
		chmod 0755 $(STAGING_DIR)/usr/bin/f2py$$v; \
	done
	# add numpy.pc backported from NumPy 2.0 (#1067398)
	mkdir -p $(STAGING_DIR)/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig
	sed -e  "s,@VERSION@,$(DEB_VERSION_UPSTREAM),g" debian/numpy.pc.in > $(STAGING_DIR)/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig/numpy.pc

execute_before_dh_gencontrol:
	python3 debian/helpers/numpy_substvars.py >> debian/python3-numpy.substvars

